#!/bin/sh
export CFLAGS="`rpm --eval '%{optflags}'` -Wall -Wimplicit -Wextra -Wuninitialized -Wno-unused -fstrict-aliasing"
libarch=`gcc --print-multi-os-directory`
usrlibarch=`cd /usr/lib/$libarch; /bin/pwd`
libarch=`basename $usrlibarch`
libtoolize -f -c
aclocal --force -I m4
autoheader
automake -f -a -i
autoconf -f -i
./configure --prefix=/usr --sysconfdir=/etc --libdir=/usr/$libarch --enable-maintainer-mode --with-server=dirsrv --with-users="cn=Users, cn=Accounts" --with-groups="cn=Groups, cn=Accounts" --enable-tests-with-memberof --enable-tests-with-refint "$@"
