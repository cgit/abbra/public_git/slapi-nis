map list:
passwd.byname
passwd.byuid
passwd.byname
passwd.byuid
contents of example.com:passwd.byname:
user1a	user1a:*:1001:1001:User 1 A:/home/user1a:/bin/sh
user1b	user1b:*:1002:1002:User 1 B:/home/user1b:/bin/sh
user1c	user1c:*:1003:1003:User 1 C:/home/user1c:/bin/sh
user2a	user2a:*:2001:2001:User 2 A:/home/user2a:/bin/sh
user2b	user2b:*:2002:2002:User 2 B:/home/user2b:/bin/sh
user2c	user2c:*:2003:2003:User 2 C:/home/user2c:/bin/sh
contents of example.com:passwd.byuid:
1001	user1a:*:1001:1001:User 1 A:/home/user1a:/bin/sh
1002	user1b:*:1002:1002:User 1 B:/home/user1b:/bin/sh
1003	user1c:*:1003:1003:User 1 C:/home/user1c:/bin/sh
2001	user2a:*:2001:2001:User 2 A:/home/user2a:/bin/sh
2002	user2b:*:2002:2002:User 2 B:/home/user2b:/bin/sh
2003	user2c:*:2003:2003:User 2 C:/home/user2c:/bin/sh
