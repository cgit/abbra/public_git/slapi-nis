dn: cn=compat,dc=example,dc=com
objectClass: extensibleObject
cn: compat

dn: cn=g1,ou=deepgroups,cn=compat,dc=example,dc=com
objectClass: posixGroup
objectClass: top
gidNumber: 1001
memberUid: tuser1
cn: g1

dn: cn=g2,ou=deepgroups,cn=compat,dc=example,dc=com
objectClass: posixGroup
objectClass: top
gidNumber: 1002
memberUid: tuser2
memberUid: tuser1
cn: g2

dn: ou=deepgroups,cn=compat,dc=example,dc=com
objectClass: extensibleObject
ou: deepgroups

